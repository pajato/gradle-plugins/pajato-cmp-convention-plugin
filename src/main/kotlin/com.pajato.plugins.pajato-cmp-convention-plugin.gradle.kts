plugins {
    id("com.pajato.plugins.pajato-kmp-convention-plugin")
    id("com.pajato.plugins.ktlint-convention-plugin")
    id("com.pajato.plugins.kover-convention-plugin")
    id("com.pajato.plugins.nexus-convention-plugin")
    id("com.pajato.plugins.publish-convention-plugin")
    id("kotlinx-serialization")
    id("org.jetbrains.dokka")
}
