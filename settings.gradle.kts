@file:Suppress("UnstableApiUsage")

rootProject.name = "pajato-cmp-convention-plugin"

pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        mavenLocal()
    }
}
