// import gradle.kotlin.dsl.accessors._aaee6d71d661078835278f56213142ec.compileKotlin
// import gradle.kotlin.dsl.accessors._aaee6d71d661078835278f56213142ec.compileTestKotlin
// import gradle.kotlin.dsl.accessors._aaee6d71d661078835278f56213142ec.test

plugins {
    // id("org.jetbrains.kotlin.jvm")
    id("org.jetbrains.kotlin.multiplatform")
}

kotlin {
    jvmToolchain { languageVersion.set(JavaLanguageVersion.of(17)) }
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks {
    compileKotlin { kotlinOptions.jvmTarget = "17" }
    compileTestKotlin { kotlinOptions.jvmTarget = "17" }
    test {
        useJUnitPlatform()
        testLogging.showStandardStreams = true
    }
}
