# pajato-cmp-convention-plugin

## Description

The standard Pajato Technologies, LLC convention plugin targeting CMP/KMP projects with support for Android, iOS and Desktlop

## Badges

We don't need no stinkin badges... :-)

## Visuals

None

## Installation

Not applicable

## Usage

Insert this into the project (top-level) file: settings.gradle.kts in the (`pluginManagement block`):

`plugins { id("com.pajato.plugins.pajato-convention-plugin") version "0.9.+" }`

## Support

You can get help by email (support@pajato.com) or by filing issues on GitLab (url goes here)

## Roadmap

tbd

## Contributing

tbd

## Authors and acknowledgments

Paul Michael Reilly
Paul Matthew Reilly

## License

GPL

## Project status

Active
