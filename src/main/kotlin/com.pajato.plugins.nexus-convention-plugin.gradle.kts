plugins {
    id("io.github.gradle-nexus.publish-plugin")
}

nexusPublishing {
    repositories {
        sonatype {
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
            username.set(project.property("SONATYPE_NEXUS_USERNAME") as String)
            password.set(project.property("SONATYPE_NEXUS_PASSWORD") as String)
        }
    }
}
