plugins {
    `kotlin-dsl`
    id("maven-publish")
}

group = "com.pajato.plugins"
version = "0.9.1"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.9.23")
    // implementation("org.jetbrains.kotlin:compose-gradle-plugin:1.6.0")
    implementation("org.jetbrains.kotlin:kotlin-serialization:1.9.23")
    implementation("org.jlleitschuh.gradle:ktlint-gradle:12.1.0")
    implementation("org.jetbrains.kotlinx:kover-gradle-plugin:0.7.6")
    implementation("io.github.gradle-nexus:publish-plugin:1.3.0")
    implementation("org.jetbrains.dokka:dokka-gradle-plugin:1.9.20")
}

publishing {
    repositories {
        mavenCentral()
        mavenLocal()
    }
}
